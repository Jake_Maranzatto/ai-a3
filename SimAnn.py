import math
import os
import numpy as np
import copy
import matplotlib.pyplot as plt
import random
from graphics import *

##DATA PROCESSING#######################################
def getData(file):
    ret = []
    data = open(file, 'r')
    for i, line in enumerate(data):
        if i > 8:
            continue
        line = line.split(' ')
        line = line[:-1]
        line = [int(i) for i in line]
        ret.append(line)
    return ret
##END DATA PROCESSING###################################

#vis
win = GraphWin('', 300,300)
for i in range(9):
    lin1 = Line(Point(i*33, 0), Point(i*33, 300))
    lin2 = Line(Point(0, i*33), Point(300, i*33))
    if i%3 == 0:
        lin1.draw(win).setWidth(3)
        lin2.draw(win).setWidth(3)
    else:
        lin1.draw(win)
        lin2.draw(win)

dataframe = []
for i in range(9):
        dataframe.append([Text(Point(j*33 + 15, i*33 + 15), 0) for j in range(9)])
        
def draw(values):
    for i in range(9):
        for j in range(9):
            dataframe[i][j].setText(values[i][j])
            if dataframe[i][j].getText() == 0:
                dataframe[i][j].undraw()
            else:
                try:
                    dataframe[i][j].draw(win)
                except:
                    continue

#end vis


#HELPER METHODS AND FINAL ALGORITHM##########

#we assume our data is input as a row-wise array of the sudoku table
def getRow(data, row):
    return data[row]

def getColumn(data, column):
    return [data[i][column] for i in range(9)]

#returns the block that the coordinate is inside of
#with possible values in the 3x3 matrix
def getInBlock(i, j):
    return (math.floor(i/3), math.floor(j/3))

#works but can make this much tighter..
def getBlock(data, x, y):
    ret = []
    for i in range(9):
        for j in range(9):
            if getInBlock(i, j) == (x, y):
                ret.append(data[i][j])
    return ret

#returns a list of the possible values a variable
#can take without making a contradiction
#essentially {0-10}/lst
def getPossibleValues(lst):
    vals = [i for i in range(1, 10)]
    return [i for i in vals if i not in lst]

#returns values that occur in all 3 lists
def restrictOverLists(list0, list1, list2):
    return [i for i in list0 if (i in list1 and i in list2)]

#returns possible values for the data at x, y
#given the column, row, & block restrictions
def getValues(data, x, y):
    pos_row = getPossibleValues(getRow(data, x))
    pos_column = getPossibleValues(getColumn(data, y))
    pos_block = getPossibleValues(getBlock(data, *getInBlock(x, y)))
    return restrictOverLists(pos_row, pos_column, pos_block)

#returns true if there are no duplicates in the list
#idea from https://stackoverflow.com/questions/1541797
def allDiff(lst):
    return len(lst) == len(set(lst))

##def ConsistencyChecker(data):
##    errors = 0
##    for i in range(9):
##        for j in range(9):
##            #if we do not have values left to assign, for whatever reason
##            if getValues(data, i, j) == []:
##                #if we have no values left because we have a completed row, column ,or block
##                if sum(getRow(data, i)) == 45 or sum(getColumn(data, j)) == 45 or sum(getBlock(data, i, j)) == 45:
##                    #and those values are consistent
##                    if allDiff([k for k in getRow(data, i) if k != 0]) and allDiff([k for k in getColumn(data,j) if k != 0]) and allDiff([k for k in getBlock(data, i, j) if k != 0]):
##                        continue
##                    else:
##                        errors += 1
##                else:
##                    if allDiff([k for k in getRow(data, i) if k != 0]) and allDiff([k for k in getColumn(data,j) if k != 0]) and allDiff([k for k in getBlock(data, i, j) if k != 0]):
##                        continue
##                    else:
##                        errors += 1
##            else:
##                continue
##            
##    return errors

def returnCondition(data):
    for i in range(9):
        for j in range(9):
            if allDiff(getRow(data, i)) and allDiff(getColumn(data, j)) and allDiff(getBlock(data, i, j)) and data[i][j] != 0:
                continue
            else:
                return False
    return True

def duplicates(lst):
    return len(lst) - len(set(lst))

def getContradictions(data, x, y):
    row = duplicates(getRow(data, x))
    col = duplicates(getColumn(data, y))
    bloc = duplicates(getBlock(data, x, y))
    return row + col+ bloc

def totalError(data):
    err = 0
    for i in range(9):
        for j in range(9):
            err += getContradictions(data, i, j)
    return err

def swapWorst(data, pivots):
    worst = (0, 0)
    secondWorst = (0, 0)
    for i in range(9):
        for j in range(9):
            if getContradictions(data, i, j) > worst[0] and pivots[i][j] == 0:
                worst = (getContradictions(data, i, j), (i,j))
                
    for i in range(9):
        for j in range(9):
            if data[i][j] == data[worst[1][0]][worst[1][1]] or pivots[i][j] == 1:
                continue
            else:
                if getContradictions(data, i, j) > secondWorst[0] and pivots[i][j] == 0:
                    secondWorst = (getContradictions(data, i, j), (i,j))
                    
    return worst[1], secondWorst[1]
                
def swap(data, a, b):
    val0 = data[a[0]][a[1]]
    data[a[0]][a[1]] = data[b[0]][b[1]]
    data[b[0]][b[1]] = val0
    return data
    
def seed(data):
    binning = [0 for i in range(9)]
    for i in range(9):
        for j in range(9):
            if data[i][j] != 0:
                binning[data[i][j] - 1] += 1
                
    
    for i in range(9):
        for j in range(9):
            
            if data[i][j] == 0:
                
                for k in range(9):
                    if binning[k] < 9:
                        binning[k] += 1
                        data[i][j] = k + 1
                        break
    
    return data

def decision(probability):
    return random.random() < probability

def simulated_Annealing(data, schedule):
    pivots = [[1 if i != 0 else 0 for i in lst] for lst in data]
    
    data = seed(data)
    for i in data:
        print(i)
    print()
    time = 0
    current = data
##algorithm body##
    while True:
        draw(current)
        currentDelta = totalError(current)
        
        time +=1
        temp = schedule(time)
       
        if temp <= 1e-10 or returnCondition(current):
            return current
        
        nextVals = swapWorst(data, pivots)
        print(nextVals)
        copy = swap(data, nextVals[0], nextVals[1])
        
        delta = totalError(copy)
        if delta < currentDelta:
            current = copy
        else:
            if decision(np.exp(-delta/temp)) == True:
                current = copy
                
    return time
        

        
            
        
                       

    

    

#END HELPER METHODS AND SOLVER#########################

#-----------------------------------------------------------------------------------------------------------#


#MAIN#
data = {}
for folder in sorted(os.listdir('./problems'), reverse = True):
    if int(folder) < 15 or int(folder) > 25:
        continue
    print(folder)
    subdata = 0
    for data_file in sorted(os.listdir('./problems/' + str(folder)), reverse = True):
        print(data_file)
        
        dat = getData('./problems/' + str(folder) + '/'+ str(data_file))
        
        x = simulated_Annealing(dat, (lambda x : 100 * (0.999**x)))
        subdata += x
    data[int(folder)] = subdata/10

data = [data[key] for key in sorted(data.keys())]
plt.plot([i for i in range(1, len(data) +1)], data)
plt.xlabel("Number of Initial Values")
plt.ylabel("Number of Variable Assignments")
plt.title("Assignments as a Function of Initial Values")
plt.show()
       
       
       





    

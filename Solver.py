import math
import os
import numpy as np
import copy
import matplotlib.pyplot as plt
import random
from graphics import *

##DATA PROCESSING#######################################
def getData(file):
    ret = []
    data = open(file, 'r')
    for i, line in enumerate(data):
        if i > 8:
            continue
        line = line.split(' ')
        line = line[:-1]
        line = [int(i) for i in line]
        ret.append(line)
    return ret
##END DATA PROCESSING###################################

#vis
win = GraphWin('', 300,300)
for i in range(9):
    lin1 = Line(Point(i*33, 0), Point(i*33, 300))
    lin2 = Line(Point(0, i*33), Point(300, i*33))
    if i%3 == 0:
        lin1.draw(win).setWidth(3)
        lin2.draw(win).setWidth(3)
    else:
        lin1.draw(win)
        lin2.draw(win)

dataframe = []
for i in range(9):
        dataframe.append([Text(Point(j*33 + 15, i*33 + 15), 0) for j in range(9)])

def draw(dat):
    for i in range(9):
        for j in range(9):
            dataframe[i][j].setText(dat[i][j])
            if dataframe[i][j].getText() == 0:
                dataframe[i][j].undraw()
            else:
                try:
                    dataframe[i][j].draw(win)
                except:
                    continue

#end vis


#HELPER METHODS AND FINAL ALGORITHM##########

#we assume our data is input as a row-wise array of the sudoku table
def getRow(data, row):
    return data[row]

def getColumn(data, column):
    return [data[i][column] for i in range(9)]

#returns the block that the coordinate is inside of
#with possible values in the 3x3 matrix
def getInBlock(i, j):
    return (math.floor(i/3), math.floor(j/3))

#works but can make this much tighter..
def getBlock(data, x, y):
    ret = []
    for i in range(9):
        for j in range(9):
            if getInBlock(i, j) == (x, y):
                ret.append(data[i][j])
    return ret

#returns a list of the possible values a variable
#can take without making a contradiction
#essentially {0-10}/lst
def getPossibleValues(lst):
    vals = [i for i in range(1, 10)]
    return [i for i in vals if i not in lst]

#returns values that occur in all 3 lists
def restrictOverLists(list0, list1, list2):
    return [i for i in list0 if (i in list1 and i in list2)]

#returns possible values for the data at x, y
#given the column, row, & block restrictions
def getValues(data, x, y):
    pos_row = getPossibleValues(getRow(data, x))
    pos_column = getPossibleValues(getColumn(data, y))
    pos_block = getPossibleValues(getBlock(data, *getInBlock(x, y)))
    return restrictOverLists(pos_row, pos_column, pos_block)


####BEGIN HUERISTICS##############################
#the flow for finding the best value is
# MRV ->MCV (tiebreak) ->LCV


#return number of variables this one constrains
#not including the variables in the block yet..
def constrainingDegree(data, i, j):
    num_row = sum([1 for m in getRow(data, i) if m == 0])
    num_column = sum([1 for m in getColumn(data, j) if m == 0])
    return num_row + num_column

#returns variable that Influences the most other variables
#using as a tiebreaker
def MCV(data, points):
    if len(points) == 1:
        return points[0]
    
    var = None
    constrictions = -1
    constraining = -1
    for point in points:
        i = point[0]
        j = point[1]
        #if we are on a free variable
        if data[i][j] == 0:
            constraints = len(getValues(data, i, j))
            #and the data is empty or we are more constricted
            if var == None or constraints < constrictions:
                #then update
                    var = (i,j)
                    constrictions = constraints
                    constraining = constrainingDegree(data, *var)
                        
    return var


#returns the most restricted variable
def MRV(data):
    var = None
    constraint = -1
    for i in range(9):
        for j in range(9):
            if data[i][j] == 0:
                if var == None or len(getValues(data, i, j)) < constraint:
                    var = [(i,j)]
                    constraint = len(getValues(data, i,j))
                elif len(getValues(data, i,j)) == constraint:
                    var.append((i,j))
    return MCV(data, var)

#naiive implementation, can be made faster
def LCV(data, i, j):
    values = getValues(data, i, j)

    lcv = {}
    for value in values:
        temp_data = copy.copy(data)
        temp_data[i][j] = value

        constrain = 0
        for x in range(9):
            for y in range(9):
                if temp_data[x][y] == 0:
                    constrain += len(getValues(temp_data, x, y))
        lcv[value] = constrain
    #print(sorted(list(lcv))[::-1])
    return sorted(list(lcv)[::-1])

####END  HUERISTICS##############################

#returns true if there are no duplicates in the list
#idea from https://stackoverflow.com/questions/1541797
def allDiff(lst):
    return len(lst) == len(set(lst))


def ConsistencyChecker(data):
    for i in range(9):
        for j in range(9):
            #if we do not have values left to assign, for whatever reason
            if getValues(data, i, j) == []:
                #if we have no values left because we have a completed row, column ,or block
                if sum(getRow(data, i)) == 45 or sum(getColumn(data, j)) == 45 or sum(getBlock(data, i, j)) == 45:
                    #and those values are consistent
                    if allDiff([k for k in getRow(data, i) if k != 0]) and allDiff([k for k in getColumn(data,j) if k != 0]) and allDiff([k for k in getBlock(data, i, j) if k != 0]):
                        continue
                    else:
                        return False
                else:
                    if allDiff([k for k in getRow(data, i) if k != 0]) and allDiff([k for k in getColumn(data,j) if k != 0]) and allDiff([k for k in getBlock(data, i, j) if k != 0]):
                        continue
                    else:
                        return False
            else:
                continue
            
    return True

def returnCondition(data):
    for i in range(9):
        for j in range(9):
            if allDiff(getRow(data, i)) and allDiff(getColumn(data, j)) and allDiff(getBlock(data, i, j)) and data[i][j] != 0:
                continue
            else:
                return False
    return True


def forwardChecker(data):
    global steps
    coordsChanged = []
    for i in range(9):
        for j in range(9):
            if data[i][j] == 0:
                if len(getValues(data, i, j)) == 1:
                    data[i][j] = getValues(data, i, j)[0]
                    steps += 1
                    print(steps)
                    draw(data)
                    if ConsistencyChecker(data):
                        coordsChanged.append((i,j))
                    else:
                        return False
    return data, coordsChanged, len(coordsChanged)
#the solver
#note that forward checking is implicit with how we structured the problem
def sudokuSolver(data):
    global steps
    print(steps)
    #initially checking to see if a real solution even exists
    #constant time operation given fixed board size.
    if returnCondition(data) == True:
        print()
        for i in data:
            print(i)
        return steps
    
    var = MRV(data)
    i = var[0]
    j = var[1]

    possible_values = LCV(data, i, j)
    for k, assignment in enumerate(possible_values):
        if steps > 10000:
            return 10000
        data[i][j] = assignment
        draw(data)
        coords_changed = []
        if ConsistencyChecker(data) == True:
            
            #using the simple forward checking routine
            backup = copy.copy(data)
            data, coords_changed, units_Taken = forwardChecker(data)
            if data != False:
                steps +=   1
                result = sudokuSolver(data)
                if result != None and result != False:
                    return result
                
            else:
                data = backup
                
        data[i][j] = 0
        for x in coords_changed:
            data[x[0]][x[1]] = 0

        
            
        
                       

    

    

#END HELPER METHODS AND SOLVER#########################

#-----------------------------------------------------------------------------------------------------------#


#MAIN#
data = {}
for folder in sorted(os.listdir('./problems'), reverse = True):
    if int(folder) < 15 or int(folder) > 25:
        continue
    print(folder)
    subdata = 0
    for data_file in sorted(os.listdir('./problems/' + str(folder)), reverse = True):
        print(data_file)
        dat = getData('./problems/' + str(folder) + '/'+ str(data_file))
        for i in dat:
            print(i)
        print()
        steps = 0
        x = sudokuSolver(dat)
        print('returned: ' + str(x))
        print()
        subdata += x
    data[int(folder)] = subdata/10

data = [data[key] for key in sorted(data.keys())]
plt.plot([i for i in range(1, len(data) +1)], data)
plt.xlabel("Number of Initial Values")
plt.ylabel("Number of Variable Assignments")
plt.title("Assignments as a Function of Initial Values")
plt.show()
       
       
       





    
